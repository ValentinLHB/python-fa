companies = [
    {
        'ticker' : 'BILI',
        'name' : 'Bilibili Inc.'
    },
    {
        'ticker' : 'CMCM',
        'name' : 'Cheetah Mobile Inc.'
    },
    {
        'ticker' : 'DOYU',
        'name' : 'DouYu International Holdings Ltd'
    },
    {
        'ticker' : 'HUYA',
        'name' : 'HUYA Inc.'
    },
    {
        'ticker' : 'MOMO',
        'name' : 'Momo Inc.'
    },
    {
        'ticker' : 'NTES',
        'name' : 'NetEase, Inc.'
    },
    {
        'ticker' : 'SINA',
        'name' : 'SINA CORP'
    },
    {
        'ticker' : 'SOHU',
        'name' : 'Sohu.com Ltd'
    },
    {
        'ticker' : 'ATVI',
        'name' : 'Activision Blizzard, Inc.'
    },
    {
        'ticker' : 'GLUU',
        'name' : 'GLU MOBILE INC'
    },
    {
        'ticker' : 'ZNGA',
        'name' : 'ZYNGA INC'
    },
    {
        'ticker' : 'LOGI',
        'name' : 'LOGITECH INTERNATIONAL S.A.'
    },
    {
        'ticker' : 'TTWO',
        'name' : 'TAKE TWO INTERACTIVE SOFTWARE INC'
    },
    {
        'ticker' : 'YY',
        'name' : 'JOYY Inc.'
    },
    {
        'ticker' : 'SNE',
        'name' : 'SONY CORP'
    },
    {
        'ticker' : 'GRVY',
        'name' : 'GRAVITY Co., Ltd.'
    },
    {
        'ticker' : 'SCPL',
        'name' : 'SciPlay Corp'
    },
    {
        'ticker' : 'EA',
        'name' : 'ELECTRONIC ARTS INC.'
    },
    {
        'ticker' : 'GME',
        'name' : 'GameStop Corp.'
    },
    {
        'ticker' : 'UBSFY',
        'name' : 'Ubisoft Entertainment SA'
    }
]