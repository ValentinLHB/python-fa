import edgar
from re import search
import pandas as pd
import lxml
from companies import companies

pd.set_option("display.max_rows", None, "display.max_columns", None)

# def main():
#     edgar.download_index(
#         "C:/Users/valen/Projects/python-fa/data",
#         2015,
#         skip_all_present_except_last=False
#     )

def get_10Q(selected_company, selected_report, year, quarter):
    '''
    
    '''
    csv = pd.read_csv(
        'data/{}-{}.tsv'.format(
            str(year), str(quarter)
        ),
        engine='python',
        sep='/t',
        lineterminator='\n',
        names=None
    )

    csv.columns.values[0] = 'Item'
    # print(csv)

    companyreport = csv[
        (csv['Item'].str.contains(selected_company)) &
        (csv['Item'].str.contains(selected_report))
    ]
    # print(companyreport)

    filing = companyreport['Item'].str.split('|')
    filing = filing.to_list()

    base_url = 'https://www.sec.gov/Archives/'

    for item_list in filing:
        for item in item_list:
            if 'html' in item:
                url = base_url + item
                # print(url)
            
                df = pd.read_html(url)
                document_index = df[0]
                document_index = document_index.dropna()
                # print(document_index)
                
                document_name = document_index[
                    document_index['Description'].str.contains(selected_report)
                ]
                document_name = document_name['Document'].str.split(' ')
                document_name = document_name[0][0]
                # print(document_name)
                # print(type(document_name))

                url_slug = item.replace('-', '').replace('index.html', '')
                # print(url_slug)

                url = base_url + url_slug + '/' + document_name
                print(url)

                dfs = pd.read_html(url)
                # print(dfs)

                for dataframe in dfs: # Check for "Total Assets" and "Total Liabilities" in each dataframe
                    dataframe[0] = dataframe[0].map(str) # convert all values to type: string
                    BS = (
                        dataframe[0].str.contains('Retained') | dataframe[0].str.contains('Total Assets')
                    ) # balance_sheet is a dataframe that should contain "retained" or "total assets"
                    if BS.any():
                        bs = dataframe # 
                        # bs[2] = bs[2].map(str)
                        # bs = bs[bs.columns[1]].map(lambda x: x.replace('(', '-'))

                        # print(bs)
                        # print('\n')
                        # print('Size: ', bs.size)
                        # print('Dimensions: ', bs.ndim)
                        # print('Shape: ', bs.shape)
                        # print('Column names: ', bs.columns)
                        
                        return bs

n = 11

print(companies[n]['name'])
print(len(companies))

selected_company = companies[n]['name']
selected_report = '10-Q'

data = get_10Q(
    selected_company,
    selected_report,
    2019,
    'QTR4'
)

print(data)

# if __name__ == "__main__":
#     main()
