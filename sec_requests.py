import requests, jsonify
from bs4 import BeautifulSoup
from pprint import pprint
import itertools

# Goal: find all 8-K / 10-K filings for a list of companies, and return the financial statement

# lists of years and quarters to get filings from
year_list = [2016, 2017, 2018, 2019, 2020, 2021]
quarter_list = ['QTR1', 'QTR2', 'QTR3', 'QTR4']

# Make all possible combinations of year and quarter
combinations_list = list(itertools.product(year_list, quarter_list))

cik = "0000718877"
filing_number = "000071887720000003"

def get_daily_index(year):
    '''
    '''
    data = requests.get(
        "https://www.sec.gov/Archives/edgar/daily-index/{}/index.json".format(
            year
        )
    ).json()
    return data

daily_index = get_daily_index(2019)
year = 2019

def get_url_list(year):
    '''
    Generates a list of all urls 
    '''
    daily_index = requests.get(
        "https://www.sec.gov/Archives/edgar/daily-index/{}/index.json".format(
            year
        )
    ).json()

    for item in daily_index['directory']['item']:

        # get the name of the folder
        print('-' * 100)
        print('Pulling url for quarter {}'.format(item['name']))

        qtr_url = "https://www.sec.gov/Archives/edgar/daily-index/{}/{}/index.json".format(
            year,
            item['name']
        )

        # request the url and decode it
        file_content = requests.get(qtr_url).json()

        print('-' * 100)
        print('Pulling files')

        file_url_list = []

        for file in file_content['directory']['item']:
            file_url = "https://www.sec.gov/Archives/edgar/daily-index/{}/{}/{}".format(
                year,
                item['name'],
                file['name']
            )
            file_url_list.append(file_url)
    
    return file_url_list
    
# url_list = get_url_list(2019)
# pprint(url_list)

file_url = r"https://www.sec.gov/Archives/edgar/daily-index/2019/QTR4/master.20191001.idx"

content = requests.get(file_url).content
pprint(content)

# with open('master_20191001.txt', 'rb') as f:
#     byte_data = f.read()

# data = byte_data.decode('utf-8').split('   ')
# print(data)

def get_archive_data(cik):
    '''
    Returns the content of the directory of the filings archive for a company, 
    given its Central Index Key (CIK). JSON format
    '''
    data = requests.get(
        "https://www.sec.gov/Archives/edgar/data/{}/index.json".format(
            cik
        )
    ).json()
    return data

def get_filing_numbers(cik):
    '''
    Returns all filing numbers for a company given its Central Index Key (CIK)
    '''
    data = requests.get(
        "https://www.sec.gov/Archives/edgar/data/{}/index.json".format(
            cik
        )
    ).json()

    filingnumber_list = []

    for filing in data['directory']['item']:

        filing_number = filing['name']
        filingnumber_list.append(filing_number)
        
    return filingnumber_list

def get_filing_contents(cik, filing_number):
    '''
    Returns the content of a single filing included in filings archive for a company,
    given its Central Index Key (CIK), and filing number. JSON format
    '''
    data = requests.get(
        "https://www.sec.gov/Archives/edgar/data/{}/{}/index.json".format(
            cik,
            filing_number
        )
    ).json()
    return data

def get_content_urls(cik, filing_number):
    '''
    Returns the documents urls contained in a filing, given the company CIK
    and filing number.
    '''
    base_url = "https://www.sec.gov/Archives/edgar/data/{}/{}/".format(
        cik,
        filing_number
    )

    get_filing_contents(
        cik,
        filing_number
    )

    url_list = []

    for document in filing_contents['directory']['item']:

        document_name = document['name']
        document_url = base_url + document_name
        url_list.append(document_url)
    
    return url_list

# data = get_archive_data(cik=cik)
# pprint(data)
# print(data['directory']['item'][0]['name'])

# filing_contents = get_filing_contents(
#     cik=cik,
#     filing_number=filing_number
# )
# pprint(filing_contents)
    
# doc_urls = get_content_urls(
#     cik=cik,
#     filing_number=filing_number
# )

# pprint(doc_urls)

# filing_numbers = get_filing_numbers(
#     cik=cik
# )
# print(filing_numbers)