import yfinance as yf
import pandas as pd
import csv
from companies import companies
from datetime import datetime
from pprint import pprint

yf.pdr_override()

ticker_list = []
info_dict = {}

# Build a list of ticker out of a dictionnary of companies
for company in companies:
    ticker_list.append(company['ticker'])
print(ticker_list)

# Get info on each company
for ticker in ticker_list:
    try:
        stock_info = yf.Ticker('{}'.format(ticker))
        d = stock_info.info
        # pprint(d)

    except Exception:
        print(Exception)

# dict = {
#     '52WeekChange': 4.6439366,
#     'SandP52WeekChange': 0.1675049,
#     'address1': 'Building 3',
#     'address2': 'Guozheng Center No. 485 Zhengli Road Yangpu District',
#     'algorithm': None,
#     'annualHoldingsTurnover': None,
#     'annualReportExpenseRatio': None,
#     'ask': 143.18,
#     'askSize': 800,
#     'averageDailyVolume10Day': 6007520,
#     'averageVolume': 6457775,
#     'averageVolume10days': 6007520,
#     'beta': 1.346006,
#     'beta3Year': None,
#     'bid': 0,
#     'bidSize': 900,
#     'bookValue': 2.677,
#     'category': None,
#     'circulatingSupply': None,
#     'city': 'Shanghai',
#     'companyOfficers': [],
#     'country': 'China',
#     'currency': 'USD',
#     'dateShortInterest': 1611878400,
#     'dayHigh': 153.57,
#     'dayLow': 143.8,
#     'dividendRate': None,
#     'dividendYield': None,
#     'earningsQuarterlyGrowth': None,
#     'enterpriseToEbitda': None,
#     'enterpriseToRevenue': None,
#     'enterpriseValue': 53791318016,
#     'exDividendDate': None,
#     'exchange': 'NMS',
#     'exchangeTimezoneName': 'America/New_York',
#     'exchangeTimezoneShortName': 'EST',
#     'expireDate': None,
#     'fiftyDayAverage': 121.42636,
#     'fiftyTwoWeekHigh': 157.66,
#     'fiftyTwoWeekLow': 19.25,
#     'fiveYearAverageReturn': None,
#     'fiveYearAvgDividendYield': None,
#     'floatShares': 158798909,
#     'forwardEps': -0.74,
#     'forwardPE': -198.43242,
#     'fromCurrency': None,
#     'fullTimeEmployees': 4791,
#     'fundFamily': None,
#     'fundInceptionDate': None,
#     'gmtOffSetMilliseconds': '-18000000',
#     'heldPercentInsiders': 0.20882,
#     'heldPercentInstitutions': 0.50838,
#     'impliedSharesOutstanding': None,
#     'industry': 'Electronic Gaming & Multimedia',
#     'isEsgPopulated': False,
#     'lastCapGain': None,
#     'lastDividendDate': None,
#     'lastDividendValue': None,
#     'lastFiscalYearEnd': 1577750400,
#     'lastMarket': None,
#     'lastSplitDate': None,
#     'lastSplitFactor': None,
#     'legalType': None,
#     'logo_url': 'https://logo.clearbit.com/bilibili.com',
#     'longBusinessSummary': 'Bilibili Inc. provides online entertainment services '
#                             "for the young generations in the People's Republic of "
#                             'China. It offers a platform that covers a range of '
#                             'genres and media formats, including videos, live '
#                             'broadcasting, and mobile games. Bilibili Inc. has a '
#                             'strategic collaboration agreement with Tencent '
#                             'Holdings Limited for sharing and operating existing '
#                             'and additional anime and games on its platform in '
#                             'China. The company was founded in 2009 and is '
#                             "headquartered in Shanghai, the People's Republic of "
#                             'China.',
#     'longName': 'Bilibili Inc.',
#     'market': 'us_market',
#     'marketCap': 50722500608,
#     'maxAge': 1,
#     'maxSupply': None,
#     'messageBoardId': 'finmb_291855394',
#     'morningStarOverallRating': None,
#     'morningStarRiskRating': None,
#     'mostRecentQuarter': 1601424000,
#     'navPrice': None,
#     'netIncomeToCommon': None,
#     'nextFiscalYearEnd': 1640908800,
#     'open': 149.29,
#     'openInterest': None,
#     'payoutRatio': 0,
#     'pegRatio': -4376.19,
#     'phone': '86 21 2509 9255',
#     'previousClose': 148.84,
#     'priceHint': 2,
#     'priceToBook': 54.852444,
#     'priceToSalesTrailing12Months': None,
#     'profitMargins': -0.25247,
#     'quoteType': 'EQUITY',
#     'regularMarketDayHigh': 153.57,
#     'regularMarketDayLow': 143.8,
#     'regularMarketOpen': 149.29,
#     'regularMarketPreviousClose': 148.84,
#     'regularMarketPrice': 149.29,
#     'regularMarketVolume': 4580896,
#     'revenueQuarterlyGrowth': None,
#     'sector': 'Communication Services',
#     'sharesOutstanding': 345427008,
#     'sharesPercentSharesOut': 0.0685,
#     'sharesShort': 23660662,
#     'sharesShortPreviousMonthDate': 1609372800,
#     'sharesShortPriorMonth': 25151120,
#     'shortName': 'Bilibili Inc.',
#     'shortPercentOfFloat': None,
#     'shortRatio': 3.23,
#     'startDate': None,
#     'strikePrice': None,
#     'symbol': 'BILI',
#     'threeYearAverageReturn': None,
#     'toCurrency': None,
#     'totalAssets': None,
#     'tradeable': False,
#     'trailingAnnualDividendRate': None,
#     'trailingAnnualDividendYield': None,
#     'trailingEps': -0.827,
#     'twoHundredDayAverage': 68.52721,
#     'volume': 4580896,
#     'volume24Hr': None,
#     'volumeAllCurrencies': None,
#     'website': 'http://www.bilibili.com',
#     'yield': None,
#     'ytdReturn': None,
#     'zip': '200433'
# }

# Get daily data from 2019 to today
# data = yf.download(
#     ticker_list,
#     start='2019-01-01',
#     end=datetime.today().strftime('%Y-%m-%d'),
#     interval='1d'
# )
# print(data.head())
