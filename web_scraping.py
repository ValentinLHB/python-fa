import bs4 as bs
import requests
import pandas as pd
import re, itertools
from pprint import pprint
from companies import companies

# lists of years and quarters to get filings from
year_list = [2016, 2017, 2018, 2019, 2020, 2021]
quarter_list = ['QTR1', 'QTR2', 'QTR3', 'QTR4']

# Make all possible combinations of year and quarter
combinations_list = list(itertools.product(year_list, quarter_list))

def get_filing_url(company, filing_type, year, quarter):
    '''
    Returns a link to a SEC Filing, given the company, filing_type, year and quarter
    of the filing

    company: String (CIK, Name)     
    filing_type: '10-Q' / '10-K'    
    year: int   
    quarter: ````QTR1``` / ````QTR2``` / ````QTR3``` / ````QTR4```  
    '''

    download = requests.get(
        f'https://www.sec.gov/Archives/edgar/full-index/{year}/{quarter}/master.idx'
    ).content
    download = download.decode("utf-8").split('\n')
    # print(download)

    for item in download:
        # Looking for company and filing type in the master file
        if company in item and filing_type in item:
            # print(item) # 1326801|Facebook Inc|10-Q|2020-07-31|edgar/data/1326801/0001326801-20-000076.txt
            company_filing = item
            company_filing = company_filing.strip()
            splitted_company_filing = company_filing.split('|')
            url = splitted_company_filing[-1]

    url2 = url.split('-') # list of strings split between - 
    url2 = url2[0] + url2[1] + url2[2] # join strings in list
    url2 = url2.split('.txt')[0] # remove .txt
    # print(url2)

    to_get_html_site = 'https://www.sec.gov/Archives/' + url
    data = requests.get(
        to_get_html_site
    ).content
    data = data.decode('utf-8')
    data = data.split('FILENAME>')
    data = data[1].split('\n')[0]

    filing_url = 'https://www.sec.gov/Archives/' + url2 + '/' + data
    # print(filing_url) # https://www.sec.gov/Archives/edgar/data/1326801/000132680120000076/fb-06302020x10q.htm
    
    return filing_url

def get_filing_url_foreign(company, filing_type, year, quarter):
    download = requests.get(
        f'https://www.sec.gov/Archives/edgar/full-index/{year}/{quarter}/master.idx'
    ).content
    download = download.decode("utf-8").split('\n')
    # print(download)

    for item in download:
        # Looking for company and filing type in the master file
        if company in item and filing_type in item:
            # print(item) # 1326801|Facebook Inc|10-Q|2020-07-31|edgar/data/1326801/0001326801-20-000076.txt
            company_filing = item
            company_filing = company_filing.strip()
            splitted_company_filing = company_filing.split('|')
            url = splitted_company_filing[-1]
        
    url2 = url.split('-') # list of strings split between - 
    url2 = url2[0] + url2[1] + url2[2] # join strings in list
    url2 = url2.split('.txt')[0] # remove .txt

    to_get_html_site = 'https://www.sec.gov/Archives/' + url

    data = requests.get(
        to_get_html_site
    ).content
    data = data.decode('utf-8')
    data = data.split('FILENAME>')
    data = data[1].split('\n')[0]

    filing_url = 'https://www.sec.gov/Archives/' + url2 + '/' + data
            
    return filing_url

print('-' * 100)
print('Starting SEC filings search')
for combination in combinations_list:
    year = combination[0]
    quarter = combination[1]
    for company in companies:
        print('\n')
        print(company['name'])
        # Get 10-K
        try:
            data = get_filing_url(
                company=company['name'],
                filing_type='10-K',
                year=year,
                quarter=quarter
            )
            print(
                '{} {} 10-K: '.format(
                    quarter, 
                    year
                ), 
                data
            )
        except Exception as e:
            print(
                'No 10-K for {} {}. Checking for 10-Q.'.format(
                    quarter,
                    year
                )
            )
        # Get 10-Q
        try:
            data = get_filing_url(
                company=company['name'],
                filing_type='10-Q',
                year=year,
                quarter=quarter
            )
            print(
                '{} {} 10-Q: '.format(
                    quarter, 
                    year
                ), 
                data
            )
        except Exception as e:
            print(
                'No 10-Q for {} {}. Company may be a foreign issuer.'.format(
                    quarter,
                    year
                )
            )
        # Get 6-K
        try:
            data_6k_foreign = get_filing_url_foreign(
                company=company['name'],
                filing_type='6-K',
                year=year,
                quarter=quarter
            )
            print(
                '6-K: {} {}'.format(
                    quarter, 
                    year
                ), 
                data_6k_foreign
            )
        except Exception as e:
            print(
                'No 6-K for {} {}. Checking foreign annual report (20-F)'.format(
                    quarter,
                    year
                )
            )
        # Get 20-F
        try:
            data_20f_foreign = get_filing_url_foreign(
                company=company['name'],
                filing_type='20-F',
                year=year,
                quarter=quarter
            )
            print(
                '20-F: {} {}'.format(
                    quarter, 
                    year
                ), 
                data_20f_foreign
            )
        except Exception as e:
            print(
                'No 20-F for {} {}'.format(
                    quarter,
                    year
                )
            )

print('-' * 100)
print('Ending SEC filing search')

# resp = requests.get(url)
# soup = bs.BeautifulSoup(resp.text, 'lxml')
# pprint(soup)

# Find the table containing balance sheet in the SEC filing
# Create an empty dictionary using the year and quarter as the key. 
# Then, a nested dictionary that will contain the balance sheet items and values.
# Loop through each of the rows in the balance sheet table. Then, get the name of the table row index (1) using row.find(“ix:nonfraction”). The html element Ix:nonfraction is the html identification that we use to extract the name of the balance sheet line item and the respective value (e.g. Cash and Cash Equivalents)
# Use try and except to get the name of the balance sheet item. It will extract the name including us-gaap. This naming convention is standardized and will be the same for all companies and years.


# https://codingandfun.com/scrape-sec-edgar-balance-sheet-python/ (pt 2)
